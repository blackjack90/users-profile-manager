package br.com.intpost.usersprofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class UsersProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersProfileApplication.class, args);
    }
}
