package br.com.intpost.usersprofile.domain.repository;

import br.com.intpost.usersprofile.domain.model.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileDatabaseRepository extends JpaRepository<Profile, Long> {

    Page<Profile> findByFirstNameContainingIgnoreCase(String firstName, Pageable pageable);

    Profile findByUsername(String username);

}
