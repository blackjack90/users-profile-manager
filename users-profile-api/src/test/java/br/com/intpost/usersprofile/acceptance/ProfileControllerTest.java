package br.com.intpost.usersprofile.acceptance;

import br.com.intpost.usersprofile.controller.helper.TokenAuthenticationHelper;
import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.util.MergeBeans;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureJson
@AutoConfigureJsonTesters
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD) // for isolated/clean database tests
public class ProfileControllerTest {

    @Autowired
    private ProfileDatabaseRepository profileRepository;

    @Autowired
    private JacksonTester<List<Profile>> jsonProfiles;
    @Autowired
    private JacksonTester<Profile> jsonProfile;

    // Populated around every test
    private Page<Profile> pageTemp = null;
    private Profile profileTemp = null;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Before
    public void setUp() {
        pageTemp = profileRepository.findAll(new PageRequest(0, 10));
        profileTemp = pageTemp.getContent().get(0);
    }

    @Test
    public void all() throws Exception {
        final Response response = given()
            .when()
            .contentType(ContentType.JSON)
            .header("Authorization", TokenAuthenticationHelper.buildJWTToken("admin"))
            .get("/api/profiles");

        response.then()
            .contentType(ContentType.JSON)
            .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void create() throws Exception {
        final Profile newProfile = Fixture.from(Profile.class).gimme("new");

        given()
            .when()
            .contentType(ContentType.JSON)
            .header("Authorization", TokenAuthenticationHelper.buildJWTToken("admin"))
            .body(jsonProfile.write(newProfile).getJson())
            .post("/api/profiles/")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(HttpStatus.SC_CREATED)
            .body("username", equalTo(newProfile.getUsername()));
    }

    @Test
    public void show() throws Exception {
        given()
            .when()
            .contentType(ContentType.JSON)
            .header("Authorization", TokenAuthenticationHelper.buildJWTToken("admin"))
            .get("/api/profiles/{id}", profileTemp.getId())
            .then()
            .contentType(ContentType.JSON)
            .statusCode(HttpStatus.SC_OK)
            .body("username", equalTo(profileTemp.getUsername()));
    }

    @Test
    public void update() throws Exception {
        final Profile profileToUpdate = Profile.builder()
            .username("gprado-edited")
            .build();

        new MergeBeans().copyProperties(profileTemp, profileToUpdate);

        given()
            .when()
            .contentType(ContentType.JSON)
            .header("Authorization", TokenAuthenticationHelper.buildJWTToken("admin"))
            .body(jsonProfile.write(profileTemp).getJson())
            .patch("/api/profiles/{id}", profileTemp.getId())
            .then()
            .contentType(ContentType.JSON)
            .statusCode(HttpStatus.SC_OK)
            .body("id", equalTo(profileTemp.getId().intValue()))
            .body("username", equalTo(profileToUpdate.getUsername()));
    }

    @Test
    public void delete() throws Exception {
        given()
            .when()
            .contentType(ContentType.JSON)
            .header("Authorization", TokenAuthenticationHelper.buildJWTToken("admin"))
            .delete("/api/profiles/{id}", profileTemp.getId())
            .then()
            .statusCode(HttpStatus.SC_NO_CONTENT);

        assertThat(profileRepository.findOne(profileTemp.getId())).isNull();
    }

}
