package br.com.intpost.usersprofile.unit.domain.service;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.domain.service.ProfileSearchService;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProfileSearchServiceTest {

    @InjectMocks
    private ProfileSearchService profileSearchServiceMock;

    @Mock
    private ProfileDatabaseRepository profileDatabaseRepositoryMock;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void searchEmpty() throws Exception {
        final Profile profile = Fixture.from(Profile.class).gimme("new");
        final Pageable pageable = new PageRequest(0, 10);
        final PageImpl page = new PageImpl(Arrays.asList(profile), pageable, 1);

        when(profileDatabaseRepositoryMock.findAll(pageable)).thenReturn(page);

        Page<Profile> profiles = profileSearchServiceMock.search("", 0);

        verify(profileDatabaseRepositoryMock).findAll(pageable);

        assertThat(profiles).isNotNull();
    }

    @Test
    public void searchWithTerm() throws Exception {
        final Profile profile = Fixture.from(Profile.class).gimme("new");
        final Pageable pageable = new PageRequest(0, 10);
        final PageImpl page = new PageImpl(Arrays.asList(profile), pageable, 1);

        when(profileDatabaseRepositoryMock.findByFirstNameContainingIgnoreCase("Terrance", pageable)).thenReturn(page);

        Page<Profile> profiles = profileSearchServiceMock.search("Terrance", 0);

        verify(profileDatabaseRepositoryMock).findByFirstNameContainingIgnoreCase("Terrance", pageable);

        assertThat(profiles).isNotNull();
    }

}