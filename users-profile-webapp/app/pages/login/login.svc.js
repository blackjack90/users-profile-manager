angular.module('app.login.svc', [])
    .service('LoginApi', ['$http', function($http) {
        this.signin = function(data, successCallback, redirectCallback, failureCallback) {
            console.log(JSON.stringify(data))
            $http.post('http://localhost:8080/login', data)
                .then(function(response) {
                    var token = response.headers('Authorization');
                    if (token)
                        successCallback(token);
                })
                .then(redirectCallback)
                .catch(function(error) {
                    if (failureCallback)
                        failureCallback(error);
                })
        }
    }]);
