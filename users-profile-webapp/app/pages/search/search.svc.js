angular.module('app.search.svc', [])
    .service('SearchUserApi', function($http) {
        this.all = function(sucessCallback, failureCallback) {
            $http.get("http://localhost:8080/api/profiles?page=0")
                .then(function(response) {
                    sucessCallback(response.data)
                }).catch(function(error) {
                    if (failureCallback)
                        failureCallback(error);
                });
        }

        this.query = function(queryTerm, page, successCallback, failureCallback) {
            if (page === "" || page === undefined || page < 0)
                page=0;
            if (queryTerm === "" || queryTerm === undefined)
                queryTerm="";

            $http.get("http://localhost:8080/api/profiles?q="+queryTerm+"&page="+page)
                .then(function(response) {
                    successCallback(response.data);
                 }).catch(function(error) {
                    if (failureCallback)
                        failureCallback(error);
                 })
        }

    });
